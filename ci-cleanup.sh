#!/bin/bash

WDIR="${1:-.}"
FILES=$(cd $WDIR; ls -1 *.rpm)
PACKAGES=$(echo -e "${FILES}" | sed \
    -e 's#\-[0-9].*\.rpm##' \
    -e 's#-RHEL.*##' | uniq)

for P in ${PACKAGES}; do
    POST=$(cd $WDIR; ls -1 "${P}-"* | cut -c $(echo $P|wc -c)- | grep -e '^-[0-9]')
    COUNT=$(echo -e "$POST" | wc -l)
    if [ ${COUNT} -ge 2 ]; then
        VERSIONS=$(echo -e "$POST" | sed -e 's#.*\([0-9]\+\.[0-9]\+\.[0-9]\+\).*#\1#' | LC_ALL=C sort -V -r | tail -n +2)
        echo "Cleanup required: ${P} --> $(echo $VERSIONS)"
        for V in ${VERSIONS}; do
            git rm ${P}-${V}-*
        done
    fi
done
