#!/bin/bash

set -e

WDIR="${1:-.}"
FILES=$(cd $WDIR; ls -1 *.rpm)
PACKAGES=$(echo $FILES | sed \
    -e 's#\-[0-9].*\.rpm##' \
    -e 's#-RHEL.*##' | uniq)

for P in ${PACKAGES}; do

    case "$P" in
        kmod-aacraid)
            echo "Skipping test for $P"
            ;;
        *)
            echo "Testing $P"
            yum install -y "$P"
            yum remove -y "$P"
            ;;
    esac
done
