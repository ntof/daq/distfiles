# nTOF distfiles

Delivery repository for nTOF applications and libraries

# Add packages

To add a new package on our nTOF Alma Linux 9 repository, just commit and push a file in al9 directory, it'll be deployed once the GitLab job is done.

# CI Tests

Packages are installed/removed one by one on a fresh CERN Alma Linux 9 to ensure this repository integrity.

Failing the CI tests will prevent new packages from being deployed.

# Install packages

To install the repository on a Alma Linux 9:
```bash
yum-config-manager --add-repo http://ntofci.web.cern.ch/ntofci/distfiles/ntof/al9/nToF.repo
```

Then directly install packages:
```bash
yum install spd-adq-pci-dkms
```
